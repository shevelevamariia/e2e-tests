export default [
  {
    title: "Тестирование главной страницы",
    slug: "main-page-test",
    isComplite: false,
    steps: [
      {
        title: "Корректность заголовка",
        description: "Название вкладки соответсвует названию университета",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Время загрузки страницы",
        description:
          "Время загрузки страницы соотвесвует всем метрикам в Lighthouse",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие контента на странице",
        description:
          "Страница является рабочей, не находится в разработке, на странице есть контент",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента поиск",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположения элемента поиск",
        description: "Элемент располагается в правом верхнем углу",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента переход на версию для слабовидящих",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположенияы элемента переход на версию для слабовидящих",
        description: "Элемент располагается в правом верхнем углу",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента переход на английскую версию",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположения элемента переход на английскую версию ",
        description: "Элемент располагается в правом верхнем углу",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента главное меню",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположения элемента главное меню ",
        description: "Элемент элемент располагается так, что пользователю не нужно листать страницу",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента логотип",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположения элемента логотип",
        description:
          "При клике на логотип осущесвляется переход на главную страницу ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Функция элемента логотип",
        description:
          "При клике на логотип осущесвляется переход на главную страницу ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие изображений",
        description: "На странице присутствуют изображения",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность отображения изображений",
        description:
          "Изображения на странице отображаются корректно, без ошибок",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Соответствие изображения контексту",
        description:
          "Изображение должно иллюстрирует расположенную на странице ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Качество изображений",
        description: "Изображения должны быть хорошего качества",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество изображений",
        description: "Страница не должна выглядет перегруженной изображениями",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество используемых видов начертания шрифтов",
        description: "На странице используется не более трёх видов начертаний",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Отсутствие рукописного шрифта",
        description:
          "Большое количесво текста не должно быть написано рукописным шрифтом",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер шрифта",
        description: "Размер шрифта дожен составлять не менее 12 px",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Цветовая гамма страницы",
        description:
          "Цветовая гамма соответствует цветовой гамме фирменного стиля",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Сочетание цветов",
        description:
          "Цвета на странице должны сочетаться, согласно цветовому кругу",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Контраст между фоном и цветом шрифта",
        description:
          "Коэффициент контраста должен сооответствовать не ниже уровню АА Large",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Поля на странице",
        description: "На странице должны присутствовать поля",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер полей",
        description: "Поля должны быть одинакого размера слева и справа",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Актуальность информации",
        description:
          "Информация распложенная на странице не является устаревшей",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Грамотность",
        description:
          "На странице отсутствуют грамматические, орфографические ошибки и фактические ошибки",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Формат иконок",
        description: "Иконки должны быть реализованы в формате SVG",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Значение иконок",
        description: "Значение иконки соотвестсвует выполняемой функции",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
    ],
  },
  {
    title: "Тестирование страниц",
    slug: "page-test",
    isComplite: false,
    steps: [
      {
        title: "Корректность заголовка",
        description: "Название вкладки соотвествет названию заголовка",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность названия страницы",
        description:
          "Название страницы соответствует названию раздела меню по которому был осуществлен переход к данной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Время загрузки страницы",
        description:
          "Время загрзки страницы соответствует всем метрикам в Lighthouse",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие контента на странице",
        description:
          "Страница является рабочей, не находится в разработке, на странице есть контент",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: 'Наличие "хлебных крошек"',
        description: 'На странице присутвуют "хлебные крошки"',
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: 'Точность "хлебных крошек"',
        description:
          'Путь указанный в "хлебных крошках" соответствует проделанному пути',
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: 'Корректность работы "хлебных крошек"',
        description: 'Ссылки в "хлебных крошках" активны',
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента поиск",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Расположение элемента поиск",
        description:
          "Элемент располагается в том же месте что и на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Цвет элемента поиск",
        description:
          "Цвет элемента соответствует цвету элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер элемента поиск",
        description:
          "Размер элемента соответствует размеру элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента переход на версию для слабовидящих",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Расположение элемента переход на версию для слабовидящих",
        description:
          "Элемент располагается в том же месте что и на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Цвет элемента переход на версию для слабовидящих",
        description:
          "Цвет элемента соответствует цвету элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер элемента переход на версию для слабовидящих",
        description:
          "Размер элемента  соответствует размеру элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента переход на английскую версию",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Расположение элемента переход на английскую версию ",
        description:
          "Элемент располагается в том же месте что и на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Цвет элемента переход на английскую версию",
        description:
          "Цвет элемента соответствует цвету элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер элемента переход на английскую версию ",
        description:
          "Размер элемента  соответствует размеру элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента главное меню",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Расположение элемента главное меню ",
        description:
          "Элемент располагается в том же месте что и на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер элемента главное меню",
        description:
          "Размер элемента  соответствует размеру элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие элемента логотип",
        description: "Элемент присутствует на странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Расположение элемента логотип",
        description:
          "Элемент располагается в том же месте что и на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Вид элемента логотип",
        description:
          "Вид элемента соответствует виду элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер элемента логотип",
        description:
          "Размер элемента  соответствует размеру элемента на главной странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Функция элемента логотип",
        description:
          "При клике на логотип осущесвляется переход на главную страницу сайта",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие изображений",
        description: "На странице присутствуют изображения",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность отображения изображений",
        description: "Название вкладки соответствует названию университета",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Соотвесвие изображения контексту",
        description:
          "Изображение должно иллюстрирует расположенную на странице ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Качество изображений",
        description: "Изображения должны быть хорошего качества",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количесво изображений",
        description: "Страница не должна выглядеть перегруженной изображениями",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количесво используемых видов начертания шрифтов",
        description: "На странице используется не более трёх видов начертаний",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Отсутствие рукописного шрифта",
        description:
          "Большое количество текста не должно быть написано рукописным шрифтом",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер шрифта",
        description: "Размер шрифта дожен составлять не менее 12 пикселей",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Цветовая гамма страницы",
        description:
          "Цветовая гамма соответствует цветовой гамме остальных страниц",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Контраст между фоном и цветом шрифта",
        description:
          "Коэффициент контраста должен сооответствовать не ниже уровню АА Large",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Поля на странице",
        description: "На странице должны присутсвовать поля",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Размер полей",
        description: "Поля должны быть одинакого размера слева и справа",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Актуальность информации",
        description:
          "Информация распложенная на странице не является устаревшей",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Грамотность",
        description:
          "На странице отсутсвуют грамматические, орфографические ошибки и фактические ошибки",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Формат иконок",
        description: "Иконки должны быть реализованы в формате SVG",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Значение иконок",
        description: "Значение иконки соответствует выполняемой функции",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
    ],
  },
  {
    title: "Проверка наличия нужной для пользователей информации",
    slug: "check-information-test",
    isComplite: false,
    steps: [
      {
        title:
          "Наличие информации о специальностях",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о специальностях",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации о преподавателях",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о преподавателях",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации о расписании занятий для всех направлений и курсов",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достиженияй информации о расписании занятий для всех направлений и курсов",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации об учебных планах для всех направлений и курсов",
        description: "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации об учебных планах для всех направлений и курсов",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации о расписании сессий для всех направлений и курсов",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения иинформации о расписании сессий для всех направлений и курсов",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о поступлении (бакалавриат, магистратура, аспирантура)",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о поступлении (бакалавриат, магистратура, аспирантура)",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации о стоимости обучения",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о стоимости обучения)",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о проходных баллах",
        description: "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о проходных баллах",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Наличие информации о количестве мест для поступления",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о количестве мест для поступлени",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о количестве мест для перевода (наличие «вакантных» мест)",
        description: "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения нформации о количестве мест для перевода (наличие «вакантных» мест)",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие на сайте образцов оформлений работ (курсовая, реферат и лабораторных)",
        description: "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения образцов оформлений работ (курсовая, реферат и лабораторных)",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие на сайте информации об олимпиадах, проводимых ВУЗом",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации об олимпиадах, проводимых ВУЗов",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие на сайте информации о размере стипендий",
        description: "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Количество кликов для достижения информации о размере стипендий",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие на сайте информации о мероприятиях, проводимых ВУЗом",
        description:
          "На сайте присутствует данная информацияю",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о мероприятиях, проводимых ВУЗов",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации об общежитиях",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации об общежитиях",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о подготовительных занятиях, проводимых в ВУЗе",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о подготовительных занятиях, проводимых в ВУЗе",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о работе служб, отделов, дирекций",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о работе служб, отделов, дирекций",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о трудоустройстве",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о трудоустройстве",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации об университете",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации об университете",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие информации о результатах приёма",
        description:
          "На сайте присутствует данная информация",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Количество кликов для достижения информации о результатах приёма",
        description: "Количество кликов не должно привышать трёх",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
    ],
  },
  {
    title: "Тестирование элементов",
    slug: "elements-test",
    isComplite: false,
    steps: [
      {
        title: "Проверка зависимости результата поиска от регистра",
        description: "Результат поиска не зависит от регистра",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title:
          "Проверка работы поиска словосочетания, когда в одном из слов допущена ошибка",
        description:
          "Результат поиска должен выдавать совпадения несмотря на ошибку, допустимо если поиск выдает совпадения по правильно написанному слову",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Проверка возможности поиска по контексту",
        description: "Поиск должен выдавать результат по контексту",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие формы обратная связь ",
        description:
          "На сайте присутствует возвожность оставить сообщение, задать вопрос",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство расположения формы обратной связи на сайте",
        description:
          "Зависит от реализации: если на отдельной странице, то на сайте должна быть очевидная ссылка, кнопа перехода или раздел меню.  Если в виде чата с пользователем, то должен находится в правом. нижнем углу. Возможно наличие обоих вариантов",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Удобство формы обратной связи",
        description:
          "Поля формы обратной связи имеют подписи, есть обязательны поля для заполнения",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие контактов учебного заведения на сайте",
        description:
          "На сайте присутвует адрес и контактный телефон учебного заведения",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие карты с расположением учебного заведения",
        description:
          "На сайте присутвует карта с обозначенным на ней учебным заведением",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Переход на английскую версию сайта",
        description:
          "Переход на английскую версию сайта происходит в той же странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Переход на русскую версию сайта с английской версии",
        description:
          "Переход на русскую версию сайта с английской происходит в той же странице",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Дизайн английской версии сайта",
        description:
          "Дизайн английской версии сайта соотвесвует дизайну русской версии ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Структура английской версии сайта",
        description:
          "Структура английской версии сайта соотвесвует русской версии",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Отсутствие русского языка на английкой версии сайта",
        description:
          "На странице должна отсутсвовать информация на русском, все элементы должны быть так же на английском",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Изменение размера шрифта в версии для слабовидящих",
        description:
          "У пользователя должна быть возможность менять размер шрифта",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Изменение цвета фона в версии для слабовидящих",
        description: "У пользователя должна быть возможность менять цвет фона",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Контраст между фоном и цветом шрифта в версии для слабовидящих",
        description:
          "Коэффициент контрастности должен соответствовать уровню ААА ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Отключение изображений в версии для слабовидящих",
        description:
          "У пользователя должна быть возможность отключать изображения",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Структура сайта в версии для слабовидящих",
        description: "Структура сайта должна оставаться прежней",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Сохранение настроек при уходе с сайта",
        description:
          "При возвращении на сайт, он должен открываться в версии для слабовидящих с сохранёнными настройками",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Сохранения настроек при переходе на другие страницы",
        description:
          "При переходе на другие страницы сайта, настройки для версии для слабовидящих должны сохранятся",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Логичность структуры меню",
        description: "Разделы меню логично скоманованы",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Наличие дополнительного разделения по группам пользователей",
        description:
          "На сайте должно быть дополнительное распредение разделов меню по группам пользователей",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Сохранение струтуры меню при изменении размера окна браузера",
        description:
          "Структура меню не должна меняться при уменьшении размеров окна браузера, все разделы рабочие",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
    ],
  },
  {
    title: "Мобильная версия",
    slug: "mobile-test",
    isComplite: false,
    steps: [
      {
        title: "Структура мобильной версии (iOS)",
        description:
          "Структура мобильной версии соотвесвует структура обычной версии сайта",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Дизайн мобильной версии (iOS)",
        description:
          "Дизайн мобильной версии соотвествует дизайну обычной версии",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Меню мобильной версии (iOS)",
        description:
          "Меню мобильной версии соответствует меню основной версии сайтаа",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Масштаб меню мобильной версии (iOS)",
        description:
          "Масштаб меню мобильной версии не нужно менять для удобного использования",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Масштаб контента мобильной версии (iOS)",
        description:
          "Масштаб контента мобильной версии не нужно менять для удобного использования",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность отображения контента (iOS)",
        description:
          "Контент мобильной версии отображается корректно без ошибок",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность работы элементов мобильной версии (iOS)",
        description:
          "Все элементы мобильной версии являются рабочими",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Структура мобильной версии (Android)",
        description:
          "Структура мобильной версии соотвесвует структура обычной версии сайта ",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Дизайн мобильной версии (Android)",
        description:
          "Дизайн мобильной версии соотвествует дизайну обычной версии",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Меню мобильной версии (Android)",
        description:
          "Меню мобильной версии соответствует меню основной версии сайтаа",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Масштаб меню мобильной версии (Android)",
        description:
          "Масштаб меню мобильной версии не нужно менять для удобного использования",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Масштаб контента мобильной версии (Android)",
        description:
          "Масштаб контента мобильной версии не нужно менять для удобного использования",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность отображения контента (Android)",
        description:
          "Контент мобильной версии отображается корректно без ошибок",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
      {
        title: "Корректность работы элементов мобильной версии (Android)",
        description:
          "Все элементы мобильной версии являются рабочими",
        status: "unexecuted",
        tickets: [],
        comment: null,
      },
    ],
  },
];
