import Vue from "vue";
import VueRouter from "vue-router";
import VueFirestore from "vue-firestore";
import App from "./App.vue";
import "tailwindcss/tailwind.css";

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueFirestore);

const router = new VueRouter({
  mode: "history",
  base: "/",
  routes: [
    {
      path: "/",
      name: "index",
      component: () => import(/* webpackChunkName: "index" */ "./App.vue"),
    },
  ],
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
