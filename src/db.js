import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "Wub220S20XTi4HppX64mVMQEljhJhDxMcks7ECHN",
  databaseURL: "https://manualtest-eb359.firebaseio.com",
  projectId: "manualtest-eb359",
};

export const db = firebase.initializeApp(firebaseConfig).firestore();

const { Timestamp, GeoPoint } = firebase.firestore;
export { Timestamp, GeoPoint };

db.settings({});
